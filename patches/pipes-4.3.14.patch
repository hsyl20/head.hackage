diff --git a/src/Pipes.hs b/src/Pipes.hs
index 7382826..b1cd820 100644
--- a/src/Pipes.hs
+++ b/src/Pipes.hs
@@ -138,7 +138,7 @@ f '~>' 'yield' = f
 @
 -}
 yield :: Functor m => a -> Producer' a m ()
-yield = respond
+yield x = respond x
 {-# INLINABLE [1] yield #-}
 
 {-| @(for p body)@ loops over @p@ replacing each 'yield' with @body@.
@@ -183,7 +183,7 @@ for = (//>)
 {-# RULES
     "for (for p f) g" forall p f g . for (for p f) g = for p (\a -> for (f a) g)
 
-  ; "for p yield" forall p . for p yield = p
+  ; "for p yield" forall p . for p (\x -> yield x) = p
 
   ; "for (yield x) f" forall x f . for (yield x) f = f x
 
@@ -637,7 +637,7 @@ next = go
 
 -- | Convert a 'F.Foldable' to a 'Producer'
 each :: (Functor m, Foldable f) => f a -> Producer' a m ()
-each = F.foldr (\a p -> yield a >> p) (return ())
+each f = F.foldr (\a p -> yield a >> p) (return ()) f
 {-# INLINABLE each #-}
 {-  The above code is the same as:
 
diff --git a/src/Pipes/Prelude.hs b/src/Pipes/Prelude.hs
index 4ab3e0f..f37e20a 100644
--- a/src/Pipes/Prelude.hs
+++ b/src/Pipes/Prelude.hs
@@ -403,12 +403,12 @@ As a result of the second law,
 > mapMaybe return = mapMaybe Just = cat
 -}
 mapMaybe :: Functor m => (a -> Maybe b) -> Pipe a b m r
-mapMaybe f = for cat $ maybe (pure ()) yield . f
+mapMaybe f = for cat $ maybe (pure ()) (\x -> yield x) . f
 {-# INLINABLE [1] mapMaybe #-}
 
 {-# RULES
     "p >-> mapMaybe f" forall p f.
-        p >-> mapMaybe f = for p $ maybe (pure ()) yield . f
+        p >-> mapMaybe f = for p $ maybe (pure ()) (\x -> yield x) . f
   #-}
 
 {-| @(filterM predicate)@ only forwards values that satisfy the monadic
@@ -453,12 +453,12 @@ As a result of the third law,
 > wither (pure . const Nothing) = wither (const (pure Nothing)) = drain
 -}
 wither :: Monad m => (a -> m (Maybe b)) -> Pipe a b m r
-wither f = for cat $ lift . f >=> maybe (pure ()) yield
+wither f = for cat $ lift . f >=> maybe (pure ()) (\x -> yield x)
 {-# INLINABLE [1] wither #-}
 
 {-# RULES
     "p >-> wither f" forall p f .
-        p >-> wither f = for p $ lift . f >=> maybe (pure ()) yield
+        p >-> wither f = for p $ lift . f >=> maybe (pure ()) (\x -> yield x)
   #-}
 
 {-| @(take n)@ only allows @n@ values to pass through
@@ -474,8 +474,8 @@ wither f = for cat $ lift . f >=> maybe (pure ()) yield
 take :: Functor m => Int -> Pipe a a m ()
 take = go
   where
-    go 0 = return () 
-    go n = do 
+    go 0 = return ()
+    go n = do
         a <- await
         yield a
         go (n-1)
@@ -555,11 +555,11 @@ dropWhile predicate = go
 
 -- | Flatten all 'Foldable' elements flowing downstream
 concat :: (Functor m, Foldable f) => Pipe (f a) a m r
-concat = for cat each
+concat = for cat (\x -> each x)
 {-# INLINABLE [1] concat #-}
 
 {-# RULES
-    "p >-> concat" forall p . p >-> concat = for p each
+    "p >-> concat" forall p . p >-> concat = for p (\x -> each x)
   #-}
 
 -- | Outputs the indices of all elements that match the given element
@@ -653,7 +653,7 @@ show = map Prelude.show
 
 -- | Evaluate all values flowing downstream to WHNF
 seq :: Functor m => Pipe a a m r
-seq = for cat $ \a -> yield $! a
+seq = for cat $ \a -> (\x -> yield x) $! a
 {-# INLINABLE seq #-}
 
 {-| Create a `Pipe` from a `ListT` transformation
@@ -663,7 +663,7 @@ seq = for cat $ \a -> yield $! a
 > loop return = cat
 -}
 loop :: Monad m => (a -> ListT m b) -> Pipe a b m r
-loop k = for cat (every . k)
+loop k = for cat (\x -> every (k x))
 {-# INLINABLE loop #-}
 
 {- $folds
@@ -926,7 +926,7 @@ zipWith :: Monad m
     -> (Producer  a m r)
     -> (Producer  b m r)
     -> (Producer' c m r)
-zipWith f = go
+zipWith f pi1 pi2 = go pi1 pi2
   where
     go p1 p2 = do
         e1 <- lift $ next p1
@@ -981,18 +981,18 @@ generalize p x0 = evalStateP x0 $ up >\\ hoist lift p //> dn
         lift $ put x
 {-# INLINABLE generalize #-}
 
-{-| The natural unfold into a 'Producer' with a step function and a seed 
+{-| The natural unfold into a 'Producer' with a step function and a seed
 
 > unfoldr next = id
 -}
-unfoldr :: Monad m 
+unfoldr :: Monad m
         => (s -> m (Either r (a, s))) -> s -> Producer a m r
 unfoldr step = go where
   go s0 = do
     e <- lift (step s0)
     case e of
       Left r -> return r
-      Right (a,s) -> do 
+      Right (a,s) -> do
         yield a
         go s
 {-# INLINABLE unfoldr #-}
