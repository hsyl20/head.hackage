diff --git a/src/Control/Monad/Free/TH.hs b/src/Control/Monad/Free/TH.hs
index 51b3349..5ad8e20 100644
--- a/src/Control/Monad/Free/TH.hs
+++ b/src/Control/Monad/Free/TH.hs
@@ -63,9 +63,8 @@ zipExprs (p:ps) cs (Param    _   : as) = p : zipExprs ps cs as
 zipExprs ps (c:cs) (Captured _ _ : as) = c : zipExprs ps cs as
 zipExprs _ _ _ = []
 
-tyVarBndrName :: TyVarBndr -> Name
-tyVarBndrName (PlainTV  name)   = name
-tyVarBndrName (KindedTV name _) = name
+tyVarBndrName :: TyVarBndr_ spec -> Name
+tyVarBndrName = elimTV id (\name _ -> name)
 
 findTypeOrFail :: String -> Q Name
 findTypeOrFail s = lookupTypeName s >>= maybe (fail $ s ++ " is not in scope") return
@@ -186,8 +185,7 @@ unifyCaptured _ xs = fail $ unlines
 extractVars :: Type -> [Name]
 extractVars (ForallT bs _ t) = extractVars t \\ map bndrName bs
   where
-    bndrName (PlainTV n) = n
-    bndrName (KindedTV n _) = n
+    bndrName = elimTV id (\n _ -> n)
 extractVars (VarT n) = [n]
 extractVars (AppT x y) = extractVars x ++ extractVars y
 #if MIN_VERSION_template_haskell(2,8,0)
@@ -202,7 +200,7 @@ extractVars (ParensT x) = extractVars x
 #endif
 extractVars _ = []
 
-liftCon' :: Bool -> [TyVarBndr] -> Cxt -> Type -> Type -> [Type] -> Name -> [Type] -> Q [Dec]
+liftCon' :: Bool -> [TyVarBndrSpec] -> Cxt -> Type -> Type -> [Type] -> Name -> [Type] -> Q [Dec]
 liftCon' typeSig tvbs cx f n ns cn ts = do
   -- prepare some names
   opName <- mkName <$> mkOpName (nameBase cn)
@@ -225,7 +223,13 @@ liftCon' typeSig tvbs cx f n ns cn ts = do
       exprs = zipExprs (map VarE xs) es args  -- this is what ctor would be applied to
       fval = foldl AppE (ConE cn) exprs       -- this is RHS without liftF
       ns' = nub (concatMap extractVars ns)
-      q = filter nonNext tvbs ++ map PlainTV (qa ++ m : ns')
+      q = filter nonNext tvbs ++ map
+#if MIN_VERSION_template_haskell(2,17,0)
+                                     (\name -> PlainTV name SpecifiedSpec)
+#else
+                                     PlainTV
+#endif
+                                     (qa ++ m : ns')
       qa = case retType of VarT b | a == b -> [a]; _ -> []
       f' = foldl AppT f ns
   return $ concat
@@ -238,11 +242,10 @@ liftCon' typeSig tvbs cx f n ns cn ts = do
         else []
     , [ FunD opName [ Clause pat (NormalB $ AppE (VarE liftF) fval) [] ] ] ]
   where
-    nonNext (PlainTV pn) = VarT pn /= n
-    nonNext (KindedTV kn _) = VarT kn /= n
+    nonNext = elimTV (\pn -> VarT pn /= n) (\kn _ -> VarT kn /= n)
 
 -- | Provide free monadic actions for a single value constructor.
-liftCon :: Bool -> [TyVarBndr] -> Cxt -> Type -> Type -> [Type] -> Maybe [Name] -> Con -> Q [Dec]
+liftCon :: Bool -> [TyVarBndrSpec] -> Cxt -> Type -> Type -> [Type] -> Maybe [Name] -> Con -> Q [Dec]
 liftCon typeSig ts cx f n ns onlyCons con
   | not (any (`melem` onlyCons) (constructorNames con)) = return []
   | otherwise = case con of
@@ -268,7 +271,7 @@ splitAppT :: Type -> [Type]
 splitAppT (AppT x y) = splitAppT x ++ [y]
 splitAppT t = [t]
 
-liftGadtC :: Name -> [BangType] -> Type -> Bool -> [TyVarBndr] -> Cxt -> Type -> Q [Dec]
+liftGadtC :: Name -> [BangType] -> Type -> Bool -> [TyVarBndrSpec] -> Cxt -> Type -> Q [Dec]
 liftGadtC cName fields resType typeSig ts cx f =
   liftCon typeSig ts cx f nextTy (init tys) Nothing (NormalC cName fields)
   where
@@ -292,6 +295,22 @@ constructorNames (RecGadtC names _ _) = names
 #endif
 constructorNames con' = fail $ "Unsupported constructor type: `" ++ pprint con' ++ "'"
 
+#if MIN_VERSION_template_haskell(2,17,0)
+type TyVarBndr_ spec = TyVarBndr spec
+#else
+type TyVarBndr_ spec = TyVarBndr
+type TyVarBndrSpec   = TyVarBndr
+#endif
+
+elimTV :: (Name -> r) -> (Name -> Kind -> r) -> TyVarBndr_ spec -> r
+#if MIN_VERSION_template_haskell(2,17,0)
+elimTV ptv _ktv (PlainTV n _)    = ptv n
+elimTV _ptv ktv (KindedTV n _ k) = ktv n k
+#else
+elimTV ptv _ktv (PlainTV n)    = ptv n
+elimTV _ptv ktv (KindedTV n k) = ktv n k
+#endif
+
 -- | Provide free monadic actions for a type declaration.
 liftDec :: Bool             -- ^ Include type signature?
         -> Maybe [Name]     -- ^ Include only mentioned constructor names. Use all constructors when @Nothing@.
