diff --git a/Distribution/Compat/Prelude.hs b/Distribution/Compat/Prelude.hs
index d032825..ecffbbc 100644
--- a/Distribution/Compat/Prelude.hs
+++ b/Distribution/Compat/Prelude.hs
@@ -140,7 +140,7 @@ import qualified Text.PrettyPrint as Disp
 import qualified Prelude as OrigPrelude
 import Distribution.Compat.Stack
 
-type IO a = WithCallStack (OrigPrelude.IO a)
+type IO a = OrigPrelude.IO a
 type NoCallStackIO a = OrigPrelude.IO a
 
 -- | New name for 'Text.PrettyPrint.<>'
diff --git a/Distribution/Compat/ReadP.hs b/Distribution/Compat/ReadP.hs
index 1f5a989..e314592 100644
--- a/Distribution/Compat/ReadP.hs
+++ b/Distribution/Compat/ReadP.hs
@@ -1,3 +1,4 @@
+{-# LANGUAGE CPP   #-}
 {-# LANGUAGE GADTs #-}
 -----------------------------------------------------------------------------
 -- |
@@ -113,7 +114,9 @@ instance Monad (P s) where
   (Result x p) >>= k = k x `mplus` (p >>= k)
   (Final r)    >>= k = final [ys' | (x,s) <- r, ys' <- run (k x) s]
 
+#if !(MIN_VERSION_base(4,13,0))
   fail = Fail.fail
+#endif
 
 instance Fail.MonadFail (P s) where
   fail _ = Fail
@@ -172,7 +175,9 @@ instance s ~ Char => Alternative (Parser r s) where
 
 instance Monad (Parser r s) where
   return = pure
+#if !(MIN_VERSION_base(4,13,0))
   fail = Fail.fail
+#endif
   R m >>= f = R (\k -> m (\a -> let R m' = f a in m' k))
 
 instance Fail.MonadFail (Parser r s) where
diff --git a/Distribution/FieldGrammar/FieldDescrs.hs b/Distribution/FieldGrammar/FieldDescrs.hs
index 4bd50d4..ce85fc0 100644
--- a/Distribution/FieldGrammar/FieldDescrs.hs
+++ b/Distribution/FieldGrammar/FieldDescrs.hs
@@ -44,7 +44,7 @@ fieldDescrPretty (F m) fn = pPretty <$> Map.lookup fn m
 
 -- | Lookup a field value parser.
 fieldDescrParse :: P.CabalParsing m => FieldDescrs s a -> String -> Maybe (s -> m s)
-fieldDescrParse (F m) fn = pParse <$> Map.lookup fn m
+fieldDescrParse (F m) fn = (\f -> pParse f) <$> Map.lookup fn m
 
 fieldDescrsToList
     :: P.CabalParsing m
diff --git a/Distribution/ParseUtils.hs b/Distribution/ParseUtils.hs
index 0e79049..f4b805c 100644
--- a/Distribution/ParseUtils.hs
+++ b/Distribution/ParseUtils.hs
@@ -19,6 +19,7 @@
 -- This module is meant to be local-only to Distribution...
 
 {-# OPTIONS_HADDOCK hide #-}
+{-# LANGUAGE CPP        #-}
 {-# LANGUAGE Rank2Types #-}
 module Distribution.ParseUtils (
         LineNo, PError(..), PWarning(..), locatedErrorMsg, syntaxError, warning,
@@ -107,7 +108,9 @@ instance Monad ParseResult where
         ParseOk ws x >>= f = case f x of
                                ParseFailed err -> ParseFailed err
                                ParseOk ws' x' -> ParseOk (ws'++ws) x'
+#if !(MIN_VERSION_base(4,13,0))
         fail = Fail.fail
+#endif
 
 instance Fail.MonadFail ParseResult where
         fail s = ParseFailed (FromString s Nothing)
diff --git a/Distribution/Parsec/Class.hs b/Distribution/Parsec/Class.hs
index d65ea54..d182360 100644
--- a/Distribution/Parsec/Class.hs
+++ b/Distribution/Parsec/Class.hs
@@ -1,3 +1,4 @@
+{-# LANGUAGE CPP                 #-}
 {-# LANGUAGE GADTs               #-}
 {-# LANGUAGE FlexibleContexts    #-}
 {-# LANGUAGE RankNTypes          #-}
@@ -55,7 +56,7 @@ class Parsec a where
 --
 -- * knows @cabal-version@ we work with
 --
-class (P.CharParsing m, MonadPlus m) => CabalParsing m where
+class (P.CharParsing m, MonadPlus m, Fail.MonadFail m) => CabalParsing m where
     parsecWarning :: PWarnType -> String -> m ()
 
     parsecHaskellString :: m String
@@ -116,7 +117,9 @@ instance Monad ParsecParser where
     (>>) = (*>)
     {-# INLINE (>>) #-}
 
+#if !(MIN_VERSION_base(4,13,0))
     fail = Fail.fail
+#endif
 
 instance MonadPlus ParsecParser where
     mzero = empty
diff --git a/Distribution/Simple/Utils.hs b/Distribution/Simple/Utils.hs
index 871a3e9..d2f45d8 100644
--- a/Distribution/Simple/Utils.hs
+++ b/Distribution/Simple/Utils.hs
@@ -1360,7 +1360,7 @@ withTempFileEx opts tmpDir template action =
     (\(name, handle) -> do hClose handle
                            unless (optKeepTempFiles opts) $
                              handleDoesNotExist () . removeFile $ name)
-    (withLexicalCallStack (uncurry action))
+    (withLexicalCallStack (\x -> uncurry action x))
 
 -- | Create and use a temporary directory.
 --
@@ -1375,7 +1375,7 @@ withTempFileEx opts tmpDir template action =
 withTempDirectory :: Verbosity -> FilePath -> String -> (FilePath -> IO a) -> IO a
 withTempDirectory verbosity targetDir template f = withFrozenCallStack $
   withTempDirectoryEx verbosity defaultTempFileOptions targetDir template
-    (withLexicalCallStack f)
+    (withLexicalCallStack (\x -> f x))
 
 -- | A version of 'withTempDirectory' that additionally takes a
 -- 'TempFileOptions' argument.
@@ -1386,7 +1386,7 @@ withTempDirectoryEx _verbosity opts targetDir template f = withFrozenCallStack $
     (createTempDirectory targetDir template)
     (unless (optKeepTempFiles opts)
      . handleDoesNotExist () . removeDirectoryRecursive)
-    (withLexicalCallStack f)
+    (withLexicalCallStack (\x -> f x))
 
 -----------------------------------
 -- Safely reading and writing files
