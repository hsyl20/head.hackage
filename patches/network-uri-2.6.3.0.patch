diff --git a/Network/URI/Static.hs b/Network/URI/Static.hs
index 7d89b01..36af670 100644
--- a/Network/URI/Static.hs
+++ b/Network/URI/Static.hs
@@ -28,7 +28,10 @@ import Language.Haskell.TH.Lib (ExpQ)
 import Language.Haskell.TH.Quote (QuasiQuoter(..))
 import Network.URI (URI(..), parseURI, parseRelativeReference)
 
-#if __GLASGOW_HASKELL__ >= 708
+#if MIN_VERSION_template_haskell(2,17,0)
+import Language.Haskell.TH.Lib (CodeQ)
+import Language.Haskell.TH.Syntax (liftCode, unTypeCode)
+#elif __GLASGOW_HASKELL__ >= 708
 import Language.Haskell.TH.Lib (TExpQ)
 import Language.Haskell.TH.Syntax (unTypeQ)
 #endif
@@ -55,9 +58,18 @@ import Language.Haskell.TH.Syntax (unTypeQ)
 -- ... Invalid URI: http://www.google.com/##
 -- ...
 staticURI :: String    -- ^ String representation of a URI
-          -> TExpQ URI -- ^ URI
+#if MIN_VERSION_template_haskell(2,17,0)
+                        -> CodeQ URI
+#else
+                        -> TExpQ URI
+#endif
+                                     -- ^ URI
 staticURI (parseURI -> Just u) = [|| u ||]
-staticURI s = fail $ "Invalid URI: " ++ s
+staticURI s =
+# if MIN_VERSION_template_haskell(2,17,0)
+  liftCode $
+# endif
+             fail $ "Invalid URI: " ++ s
 #endif
 
 -- | 'staticURI'' parses a specified string at compile time.
@@ -65,7 +77,9 @@ staticURI s = fail $ "Invalid URI: " ++ s
 -- The typed template haskell 'staticURI' is available only with GHC-7.8+.
 staticURI' :: String    -- ^ String representation of a URI
            -> ExpQ      -- ^ URI
-#if __GLASGOW_HASKELL__ >= 708
+#if MIN_VERSION_template_haskell(2,17,0)
+staticURI' = unTypeCode . staticURI
+#elif __GLASGOW_HASKELL__ >= 708
 staticURI' = unTypeQ . staticURI
 #else
 staticURI' (parseURI -> Just u) = [| u |]
@@ -108,9 +122,18 @@ uri = QuasiQuoter {
 -- ... Invalid relative reference: http://www.google.com/
 -- ...
 staticRelativeReference :: String -- ^ String representation of a reference
-                        -> TExpQ URI -- ^ Refererence
+#if MIN_VERSION_template_haskell(2,17,0)
+                        -> CodeQ URI
+#else
+                        -> TExpQ URI
+#endif
+                                      -- ^ Refererence
 staticRelativeReference (parseRelativeReference -> Just ref) = [|| ref ||]
-staticRelativeReference ref = fail $ "Invalid relative reference: " ++ ref
+staticRelativeReference ref =
+# if MIN_VERSION_template_haskell(2,17,0)
+  liftCode $
+# endif
+             fail $ "Invalid relative reference: " ++ ref
 #endif
 
 -- | 'staticRelativeReference'' parses a specified string at compile time and
@@ -120,7 +143,9 @@ staticRelativeReference ref = fail $ "Invalid relative reference: " ++ ref
 -- The typed template haskell 'staticRelativeReference' is available only with GHC-7.8+.
 staticRelativeReference' :: String -- ^ String representation of a reference
                          -> ExpQ   -- ^ Refererence
-#if __GLASGOW_HASKELL__ >= 708
+#if MIN_VERSION_template_haskell(2,17,0)
+staticRelativeReference' = unTypeCode . staticRelativeReference
+#elif __GLASGOW_HASKELL__ >= 708
 staticRelativeReference' = unTypeQ . staticRelativeReference
 #else
 staticRelativeReference' (parseRelativeReference -> Just ref) = [| ref |]
