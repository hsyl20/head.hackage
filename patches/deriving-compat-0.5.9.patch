diff --git a/src/Data/Deriving/Internal.hs b/src/Data/Deriving/Internal.hs
index 9f19381..1462c70 100644
--- a/src/Data/Deriving/Internal.hs
+++ b/src/Data/Deriving/Internal.hs
@@ -813,14 +813,12 @@ newNameList :: String -> Int -> Q [Name]
 newNameList prefix n = mapM (newName . (prefix ++) . show) [1..n]
 
 -- | Extracts the kind from a TyVarBndr.
-tvbKind :: TyVarBndr -> Kind
-tvbKind (PlainTV  _)   = starK
-tvbKind (KindedTV _ k) = k
+tvbKind :: TyVarBndr_ spec -> Kind
+tvbKind = elimTV (\_ -> starK) (\_ k -> k)
 
 -- | Convert a TyVarBndr to a Type.
-tvbToType :: TyVarBndr -> Type
-tvbToType (PlainTV n)    = VarT n
-tvbToType (KindedTV n k) = SigT (VarT n) k
+tvbToType :: TyVarBndr_ spec -> Type
+tvbToType = elimTV VarT (\n k -> SigT (VarT n) k)
 
 -- | Applies a typeclass constraint to a type.
 applyClass :: Name -> Name -> Pred
@@ -1045,7 +1043,7 @@ tag2ConExpr ty = do
     let tvbs = avoidTypeInType $ freeVariablesWellScoped [ty']
     lam1E (conP iHashDataName [varP iHash]) $
         varE tagToEnumHashValName `appE` varE iHash
-            `sigE` return (ForallT tvbs [] ty')
+            `sigE` return (ForallT (changeSpecs SpecifiedSpec tvbs) [] ty')
             -- tagToEnum# is a hack, and won't typecheck unless it's in the
             -- immediate presence of a type ascription like so:
             --
@@ -1072,16 +1070,16 @@ tag2ConExpr ty = do
     -- a breaking change, so I decided against it at the time. If we ever make
     -- some breaking change in the future, however, this would be at the top
     -- of the list of things that I'd rip out.
-    avoidTypeInType :: [TyVarBndr] -> [TyVarBndr]
+    avoidTypeInType :: [TyVarBndrUnit] -> [TyVarBndrUnit]
 #if __GLASGOW_HASKELL__ >= 806
     avoidTypeInType = id
 #else
     avoidTypeInType = go . map attachFreeKindVars
       where
-        attachFreeKindVars :: TyVarBndr -> (TyVarBndr, [Name])
+        attachFreeKindVars :: TyVarBndrUnit -> (TyVarBndrUnit, [Name])
         attachFreeKindVars tvb = (tvb, freeVariables (tvKind tvb))
 
-        go :: [(TyVarBndr, [Name])] -> [TyVarBndr]
+        go :: [(TyVarBndrUnit, [Name])] -> [TyVarBndrUnit]
         go [] = []
         go ((tvb, _):tvbsAndFVs)
           | any (\(_, kindVars) -> tvName tvb `elem` kindVars) tvbsAndFVs
@@ -1219,6 +1217,34 @@ ghc7'8OrLater = True
 ghc7'8OrLater = False
 #endif
 
+#if MIN_VERSION_template_haskell(2,17,0)
+type TyVarBndr_ spec = TyVarBndr spec
+#else
+type TyVarBndr_ spec = TyVarBndr
+type TyVarBndrSpec   = TyVarBndr
+type TyVarBndrUnit   = TyVarBndr
+
+data Specificity
+  = SpecifiedSpec
+  | InferredSpec
+#endif
+
+elimTV :: (Name -> r) -> (Name -> Kind -> r) -> TyVarBndr_ spec -> r
+#if MIN_VERSION_template_haskell(2,17,0)
+elimTV ptv _ktv (PlainTV n _)    = ptv n
+elimTV _ptv ktv (KindedTV n _ k) = ktv n k
+#else
+elimTV ptv _ktv (PlainTV n)    = ptv n
+elimTV _ptv ktv (KindedTV n k) = ktv n k
+#endif
+
+changeSpecs :: newSpec -> [TyVarBndr_ oldSpec] -> [TyVarBndr_ newSpec]
+#if MIN_VERSION_template_haskell(2,17,0)
+changeSpecs newSpec = map (newSpec <$)
+#else
+changeSpecs _ = id
+#endif
+
 -------------------------------------------------------------------------------
 -- Manually quoted names
 -------------------------------------------------------------------------------
diff --git a/src/Data/Deriving/Via/Internal.hs b/src/Data/Deriving/Via/Internal.hs
index 2bbea7e..48d9f50 100644
--- a/src/Data/Deriving/Via/Internal.hs
+++ b/src/Data/Deriving/Via/Internal.hs
@@ -133,7 +133,7 @@ deriveViaDecs instanceTy mbViaTy = do
         _ -> fail $ "Not a type class: " ++ pprint clsTy
     _ -> fail $ "Malformed instance: " ++ pprint instanceTy
 
-deriveViaDecs' :: Name -> [TyVarBndr] -> [Type] -> Type -> Dec -> Q (Maybe [Dec])
+deriveViaDecs' :: Name -> [TyVarBndrUnit] -> [Type] -> Type -> Dec -> Q (Maybe [Dec])
 deriveViaDecs' clsName clsTvbs clsArgs repTy dec = do
     let numExpectedArgs = length clsTvbs
         numActualArgs   = length clsArgs
@@ -172,7 +172,7 @@ deriveViaDecs' clsName clsTvbs clsArgs repTy dec = do
 
     go _ = return Nothing
 
-mkCoerceClassMethEqn :: [TyVarBndr] -> [Type] -> Type -> Type -> (Type, Type)
+mkCoerceClassMethEqn :: [TyVarBndrUnit] -> [Type] -> Type -> Type -> (Type, Type)
 mkCoerceClassMethEqn clsTvbs clsArgs repTy methTy
   = ( applySubstitution rhsSubst methTy
     , applySubstitution lhsSubst methTy
@@ -181,7 +181,7 @@ mkCoerceClassMethEqn clsTvbs clsArgs repTy methTy
     lhsSubst = zipTvbSubst clsTvbs clsArgs
     rhsSubst = zipTvbSubst clsTvbs $ changeLast clsArgs repTy
 
-zipTvbSubst :: [TyVarBndr] -> [Type] -> Map Name Type
+zipTvbSubst :: [TyVarBndr_ spec] -> [Type] -> Map Name Type
 zipTvbSubst tvbs = M.fromList . zipWith (\tvb ty -> (tvName tvb, ty)) tvbs
 
 -- | Replace the last element of a list with another element.
@@ -199,7 +199,7 @@ stripOuterForallT (ForallT _ _ ty) = ty
 #endif
 stripOuterForallT ty               = ty
 
-decomposeType :: Type -> ([TyVarBndr], Cxt, Type)
+decomposeType :: Type -> ([TyVarBndrSpec], Cxt, Type)
 decomposeType (ForallT tvbs ctxt ty) = (tvbs, ctxt, ty)
 decomposeType ty                     = ([],   [],   ty)
 
diff --git a/src/Data/Functor/Deriving/Internal.hs b/src/Data/Functor/Deriving/Internal.hs
index e1d250d..b5ad035 100644
--- a/src/Data/Functor/Deriving/Internal.hs
+++ b/src/Data/Functor/Deriving/Internal.hs
@@ -727,7 +727,7 @@ data FFoldType a      -- Describes how to fold over a Type in a functor like way
           --   @arg_ty@ in @fun_ty arg_ty@.
         , ft_bad_app :: a
           -- ^ Type app, variable other than in last argument
-        , ft_forall  :: [TyVarBndr] -> a -> a
+        , ft_forall  :: [TyVarBndrSpec] -> a -> a
           -- ^ Forall type
      }
 
